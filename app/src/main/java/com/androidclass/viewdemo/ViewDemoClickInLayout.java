package com.androidclass.viewdemo;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by philip on 5/27/17.
 */

public class ViewDemoClickInLayout extends Activity {

    EditText name;
    TextView textHello;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lauout_click_in_layout);

        name = (EditText) findViewById(R.id.edit_name);
        textHello= (TextView) findViewById(R.id.text_hello);
    }


    public void sayHello(View button){
        String userName= name.getText().toString();
        textHello.setText("Hello, "+userName);



    }



}
