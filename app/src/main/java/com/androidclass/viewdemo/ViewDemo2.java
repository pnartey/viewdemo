package com.androidclass.viewdemo;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

/**
 * Created by philip on 6/3/17.
 */

public class ViewDemo2 extends AppCompatActivity {
    EditText search;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.widget_layout_2);
        search= (EditText) findViewById(R.id.search);

        Intent intent= getIntent();
        search.setText(intent.getStringExtra("USER_NAME"));
    }
}
