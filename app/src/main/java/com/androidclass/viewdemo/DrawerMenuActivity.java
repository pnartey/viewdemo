package com.androidclass.viewdemo;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

/**
 * Created by philip on 6/10/17.
 */

public class DrawerMenuActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    DrawerLayout drawerLayout;
    Toolbar  toolbar;
    NavigationView navigationView;

    ActionBarDrawerToggle drawerToggle;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_drawer_menu);

        drawerLayout= (DrawerLayout) findViewById(R.id.drawer_layout);
        toolbar= (Toolbar) findViewById(R.id.toolbar);
        navigationView= (NavigationView) findViewById(R.id.navigation_view);


        setSupportActionBar(toolbar);
        drawerToggle= new ActionBarDrawerToggle(this, drawerLayout,toolbar, R.string.open, R.string.close);
        drawerLayout.addDrawerListener(drawerToggle);
        navigationView.setNavigationItemSelectedListener(this);

        drawerToggle.syncState();
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_view_demo:
                //TODO Navigate to view demo
                Intent intentViewDemo= new Intent(this, ViewDemo.class);
                startActivity(intentViewDemo);
                break;
            case R.id.menu_view_demo_2:
                Intent intentViewDemo2= new Intent(this, ViewDemo2.class);
                startActivity(intentViewDemo2);
                break;
            case R.id.menu_view_demo_click:
                Intent intentViewDemoClick= new Intent(this, ViewDemoClickInLayout.class);
                startActivity(intentViewDemoClick);
                break;
            case R.id.menu_custom_toolbar:
                Intent intentCustomToolBar= new Intent(this, CustomTooBarACtivity.class);
                startActivity(intentCustomToolBar);
                break;
            case R.id.menu_test_fragment:
                Intent intentTestFragment= new Intent(this, TestFragmentActivity.class);
                startActivity(intentTestFragment);
                break;
        }
        drawerLayout.closeDrawer(GravityCompat.START);
        return false;
    }
}
