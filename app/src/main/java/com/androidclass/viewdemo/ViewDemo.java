package com.androidclass.viewdemo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by philip on 5/27/17.
 */

public class ViewDemo extends Activity implements View.OnClickListener {

    EditText name;
    TextView textHello;
    CheckBox checkLikeOurApp;
    TextView likeOurApp;
    Button btnActivity2;
    RelativeLayout layout_parent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.widget_layout);

        name = (EditText) findViewById(R.id.edit_name);
        textHello= (TextView) findViewById(R.id.text_hello);
        Button sayHello= (Button)findViewById(R.id.say_hello);
        checkLikeOurApp= (CheckBox) findViewById(R.id.check_o_you_like);
        likeOurApp = (TextView) findViewById(R.id.like_our_aapp);
        btnActivity2 = (Button) findViewById(R.id.btnActivity2);
        layout_parent= (RelativeLayout) findViewById(R.id.layout_parent);


        btnActivity2.setOnClickListener(this);
        sayHello.setOnClickListener(this);
        checkLikeOurApp.setOnClickListener(this);


//        sayHello.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String userName= name.getText().toString();
//                textHello.setText("Hello, "+userName);
//            }
//        });

    }

    @Override
    public void onClick(View v) {
        String userName= name.getText().toString();
        Snackbar.make(layout_parent, userName, Snackbar.LENGTH_SHORT).show();

        switch (v.getId()){
            case R.id.say_hello:
                textHello.setText("Hello, "+userName);
                break;
            case R.id.check_o_you_like:
                if(checkLikeOurApp.isChecked()){
                    likeOurApp.setText("Thank you "+userName+" for liking our app");
                }
                else {
                    likeOurApp.setText("");
                }
                break;
            case R.id.btnActivity2:
                Intent intent=new Intent(this, ViewDemo2.class);
                intent.putExtra("USER_NAME", userName);
                startActivity(intent);
                break;
        }
//        if(v.getId()==R.id.say_hello){
//            textHello.setText("Hello, "+userName);
//        }
//        else {
//            if(v.getId()==R.id.check_o_you_like){
//                if(checkLikeOurApp.isChecked()){
//                    likeOurApp.setText("Thank you "+userName+" for liking our app");
//                }
//                else {
//                    likeOurApp.setText("");
//                }
//            }
//        }
    }

}
