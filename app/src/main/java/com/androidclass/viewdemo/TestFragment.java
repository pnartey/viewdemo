package com.androidclass.viewdemo;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by philip on 6/10/17.
 */

public class TestFragment extends Fragment {

    TextView fragmentText;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.layout_test_fragment, null );

        return  view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
       fragmentText= (TextView) view.findViewById(R.id.fragment_text);
        if(getArguments()!=null){
            fragmentText.setText(getArguments().getString("POSITION"));
        }


    }
}
