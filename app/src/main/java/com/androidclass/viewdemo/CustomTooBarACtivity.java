package com.androidclass.viewdemo;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;

/**
 * Created by philip on 6/10/17.
 */

public class CustomTooBarACtivity extends AppCompatActivity {

    Toolbar toolbar;
    LinearLayout layoutContainer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_custom_tool_bar);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        layoutContainer= (LinearLayout) findViewById(R.id.layout_container);

        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle(R.string.customToolBar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.custom_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
            case R.id.menu_search:
                Snackbar.make(layoutContainer, "User selected Search", Snackbar.LENGTH_SHORT).show();
                break;
            case R.id.menu_add:
                Snackbar.make(layoutContainer, "User selected add", Snackbar.LENGTH_SHORT).show();
                break;
            case R.id.menu_edit:
                Snackbar.make(layoutContainer, "User selected edit", Snackbar.LENGTH_SHORT).show();
                break;
            case R.id.menu_delete:
                Snackbar.make(layoutContainer, "User selected delete", Snackbar.LENGTH_SHORT).show();
                break;
        }

        return true;
    }




}
