package com.androidclass.viewdemo;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by philip on 6/10/17.
 */

public class TestFragmentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_fragment_activity);


        FragmentManager fragmentManager= getFragmentManager();

        FragmentTransaction fragmentTransaction= fragmentManager.beginTransaction();
        TestFragment testFragmentTop= new TestFragment();
        Bundle topBundle= new Bundle();
        topBundle.putString("POSITION", "TOP FRAGMENT");
        testFragmentTop.setArguments(topBundle);
        fragmentTransaction.add(R.id.topFragment, testFragmentTop, "TEST_FRAGMENT_TOP");

        TestFragment testFragmentDown = new TestFragment();
        Bundle  downBundle= new Bundle();
        downBundle.putString("POSITION", "DOWN FRAGMENT");
        testFragmentDown.setArguments(downBundle);
        fragmentTransaction.add(R.id.downFragment, testFragmentDown, "TEST_FRAGMENT_DOWN");

        fragmentTransaction.commit();
    }
}
